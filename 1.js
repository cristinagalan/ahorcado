/* ****************************************************************************************** */
/* ****************************************************************************************** */
/* *********************************** JUEGO DEL AHORCADO *********************************** */
/* ****************************************************************************************** */
/* ****************************************************************************************** */


/* *********************************** VARIABLES *********************************** */

var palabras = ["caracter", "hola", "agua", "javascript", "coche"]; // debe contener las palabras en MINÚSCULAS
var longitud; //longitud de la palabra
var posicion; //posición en el array palabras de la palabra con la que vamos a jugar
var c = ""; // caracter introducido
var lIntroducidas = []; // array que contiene las letras introducidas;
var acertaste = 0; //almacena el número de letras que acierta para saber cuando ha acertado la palabra completa
var posiciones = []; //almacena las posiciones donde coincida el carácter
var acumulador = 0; // Almacena el número de coincidencias
var contador = 0;


/* *********************************** (1) ALEATORIO *********************************** */

/* Devuelve un número entre 0 y la longitud del array de palabras que es la posición de la palabra aleatoria*/

function aleatorio(arg) { 
// Recibe el array que contiene las palabras (palabras)
    var x = 0; // almacena la posición de la palabra aleatoria
    var l = 0; // longitud del array

    l = arg.length;
    x = parseInt(Math.random() * l);
    return x;

}


/* *********************************** (2) CARGAR *********************************** */

function cargar() {
    posicion = aleatorio(palabras); //posición de la palabra elegida al azar

    for (var i = 1; i < 6; i++) {
        document.querySelector("#id" + i).style.opacity = 1 //Tapar el muñeco
    }
    /* Longitud de la palabra */

    longitud = palabras[posicion].length; //Longitud de la palabra

    dibujar(longitud);
}

 /* *********************************** (3) DIBUJAR CAJAS *********************************** */
    /* Dibujamos tantas cajas como letras tenga la palabra*/

    function dibujar(arg) { //Recibe el nº de cajas a dibujar
        var v = []; //array de divs

        v = document.querySelector(".fila1")
        for (var i = 0; i < longitud; i++) {
            v.innerHTML += "<div class='letras'></div>";

        }
    }
/* *********************************** (4) LEER CARACTER INTRODUCIDO *********************************** */

/* Creamos la función que lee los caracteres */


function leer() {

    c = (document.querySelector("#letra").value).toLowerCase(); // leo el valor. Lo paso a minúsculas
    document.querySelector("#letra").value = ""; //limpio el input
    comprobar(c); //compruebo la letra  
}



/* *********************************** (5) COMPROBAR *********************************** */


function comprobar(arg) { //Recibe la letra que introduce el usuario
    var si = estaIntroducida(c); // si devuelve 1 está introducida
    var str = ""; //contiene la palabra con la que jugamos
    var contador = 0; //Almacena cuántas veces no coincide la letra en el string

    str = palabras[posicion];

 // ¿YÁ ESTÁ INTRODUCIDO?
    
    /* Letra ya introducida */

    if (si == 1) {
        alert("La letra ya está introducida.");
    } else {

    /* Letra no introducida (nueva) */

        // Compruebo si está el carácter en la palabra y guardo las posiciones
        for (var i = 0; i < longitud; i++) { //Almacena en el array posiciones, las posiciones donde coincida el carácter
            if (arg == str[i]) { //coincide la letra
                posiciones.push(i);
                acertaste++;

            } else { //no coincide la letra
                contador++;
            }

        }
// ¿ESTÁ CONTENIDO EN LA PALABRA? 
 
        if (contador == longitud) {
            escribirFallos(arg); //si la letra no está contenida en la palabra la escribe en fallos

        } else {
            escribirAciertos(arg, posiciones);
        }

        lIntroducidas.push(arg);

        /*  fin del juego acertaste*/

        if (acertaste == longitud) {
            setTimeout(function() {
                alert("¡Acertaste la palabra!");
                location.reload();
            }, 1000);

        }
    }
}



/* *********************************** (6) ¿ESTÁ INTRODUCIDO? *********************************** */

/* Compruebo si la letra está introducida */
function estaIntroducida(arg) { // Recibe el caracter y comprueba si está introducida
    for (var i = 0; i < lIntroducidas.length; i++) {
        if (arg == lIntroducidas[i]) {
            return 1;
        }
    }

}


/* *********************************** (7) ESCRIBIR ACIERTOS *********************************** */

/* Función para escribir los caracteres que acierta el usuario */

function escribirAciertos(arg1, arg2) {
    //Recibe:
        // arg1 es la letra que coincide, 
        // arg2 es el array con las posiciones de la letra de arg1
    
    var a = 0; //almacena la posición del array de posiciones

    for (var i = 0; i < arg2.length; i++) {
        var a = arg2[i];
        var b = document.querySelectorAll(".letras");

        b[a].innerHTML += arg1.toUpperCase();
        b[a].style.backgroundColor = "#2ECC71";
    }

   // contador = arg2.length;
    posiciones = []; //limpiar el array

}

/* *********************************** (8) ESCRIBIR FALLOS *********************************** */

/* Función para escribir los caracteres que falla el usuario */

var f = 0; //posiciones de las cajas de los fallos

function escribirFallos(arg) {
    v = document.querySelector(".fila2");
    v.innerHTML += "<div class='letrasF'></div>";
    document.querySelectorAll(".letrasF")[f].innerHTML += arg.toUpperCase();

    f++;


    if (f < 6) {
        descubrir(f);
    }
    /* PERDISTE */
    //Si tienes 5 fallos perdiste
    if (f == 5) {
        document.querySelector("#id5").style.opacity = 0;
        setTimeout(function() {
            alert("Perdiste");
            location.reload();
        }, 1000);
    }

}

/* *********************************** (9) DESCUBRIR IMAGEN *********************************** */
function descubrir(arg) {
    document.querySelector("#id" + arg).style.opacity = 0;

}